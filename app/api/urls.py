from django.urls import path

from app.api.views import \
    TransferDataAPIView, \
    DeleteCompanyAPIView, \
    EditCompanyAPIView, \
    CreateCompanyAPIView, \
    DeleteUserAPIView, \
    EditUserAPIView, \
    CreateUserAPIView, \
    CompanyListAPIView, \
    UserListAPIView

urlpatterns = [
    path('users/', UserListAPIView.as_view(), name='users'),
    path('companies/', CompanyListAPIView.as_view(), name='companies'),
    path('abusers', TransferDataAPIView.as_view(), name='abusers'),
    path('create-user', CreateUserAPIView.as_view(), name='create-user'),
    path('edit-user/<str:pk>', EditUserAPIView.as_view(), name='edit-user'),
    path('delete-user/<str:pk>', DeleteUserAPIView.as_view(), name='delete-user'),
    path('create-company', CreateCompanyAPIView.as_view(), name='create-company'),
    path('edit-company/<str:pk>', EditCompanyAPIView.as_view(), name='edit-company'),
    path('delete-company/<str:pk>', DeleteCompanyAPIView.as_view(), name='delete-company'),
]
