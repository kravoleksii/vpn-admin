import time

from django.db import IntegrityError
from django.db.models import Sum, F
from django.db.models.functions import ExtractMonth, ExtractYear
from rest_framework import status
from rest_framework.generics import ListAPIView, CreateAPIView, UpdateAPIView, DestroyAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from app.api.serializers import UserListSerializer, UserSerializer, CompanySerializer, CompanyAbusersSerializer
from app.models import User, Company, TransferData


class UserListAPIView(ListAPIView):
    serializer_class = UserListSerializer
    queryset = User.objects.order_by('-id')


class CreateUserAPIView(CreateAPIView):
    serializer_class = UserSerializer


class EditUserAPIView(UpdateAPIView):
    serializer_class = UserSerializer
    queryset = User.objects.all()


class DeleteUserAPIView(DestroyAPIView):
    queryset = User.objects.all()


class CompanyListAPIView(ListAPIView):
    serializer_class = CompanySerializer
    queryset = Company.objects.order_by('-id')


class CreateCompanyAPIView(CreateAPIView):
    serializer_class = CompanySerializer


class EditCompanyAPIView(UpdateAPIView):
    serializer_class = CompanySerializer
    queryset = Company.objects.all()


class DeleteCompanyAPIView(DestroyAPIView):
    queryset = Company.objects.all()


class TransferDataAPIView(APIView):
    def get(self, request):
        month = int(request.query_params['month'])
        year = int(request.query_params['year'])
        query = Company.objects.annotate(
            used=Sum('user__transferdata__transferred_data'),
            month=ExtractMonth("user__transferdata__published_at"),
            year=ExtractYear("user__transferdata__published_at")
        ).filter(
            used__gt=F('quota'),
            month=month,
            year=year
        ).order_by('-used')
        companies_data = CompanyAbusersSerializer(instance=query, many=True).data
        return Response(companies_data)

    def post(self, request):
        TransferData.generate_fake_data()
        return Response('')
