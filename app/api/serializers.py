from rest_framework.fields import SerializerMethodField, IntegerField
from rest_framework.serializers import ModelSerializer

from app.models import User, Company


class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'name', 'lastname', 'email', 'company']


class UserListSerializer(ModelSerializer):
    company = SerializerMethodField()

    class Meta:
        model = User
        fields = ['id', 'name', 'lastname', 'email', 'company']

    def get_company(self, instance: User):
        return CompanySerializer(instance.company).data


class CompanySerializer(ModelSerializer):
    class Meta:
        model = Company
        fields = ['id', 'name', 'quota']


class CompanyAbusersSerializer(ModelSerializer):
    used = IntegerField()

    class Meta:
        model = Company
        fields = ['id', 'name', 'quota', 'used']


