from django.contrib import admin
from django.contrib.admin import ModelAdmin

from .models import Company, User, TransferData

models_list = [
    Company,
    User
]

admin.site.register(models_list)


@admin.register(TransferData)
class TransferDataAdmin(ModelAdmin):
    list_display = ['user', 'published_at', 'transferred_data']
