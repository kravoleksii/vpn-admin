from datetime import timedelta

from django.db import models
from django.utils import timezone
from faker import Faker


class Company(models.Model):
    class Meta:
        verbose_name_plural = 'Companies'

    name = models.CharField(max_length=100, unique=True)
    quota = models.IntegerField()

    def __str__(self) -> str:
        return self.name


class User(models.Model):
    name = models.CharField(max_length=100, unique=True)
    lastname = models.CharField(max_length=100, unique=True)
    email = models.EmailField(blank=True, unique=True)
    company = models.ForeignKey(Company, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class TransferData(models.Model):
    class Meta:
        verbose_name_plural = 'Transfers data'
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="transferdata")
    published_at = models.DateTimeField()
    resource = models.URLField()
    transferred_data = models.IntegerField()

    def __str__(self):
        return self.user.name

    @staticmethod
    def generate_fake_data():

        faker = Faker()

        data_to_create = []

        for user in User.objects.all():  # user
            # create date range with timedelta of 1 month
            end_date = timezone.now()
            delta = timedelta(30)  # 1 month
            start_date = end_date - delta

            for _ in range(1, 7):
                for _ in range(8, 84):
                    data = TransferData(
                        user=user,
                        published_at=faker.date_time_between_dates(start_date, end_date),
                        resource=faker.image_url(),
                        transferred_data=faker.random_int(100, 1024 ** 3)
                    )
                    data_to_create.append(data)
                # shift date range with timedelta every iteration
                end_date = start_date
                start_date -= delta

        TransferData.objects.bulk_create(data_to_create)
