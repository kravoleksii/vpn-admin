import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CompaniesComponent} from './pages/companies/companies.component';
import {UsersComponent} from './pages/users/users.component';
import {AbusersComponent} from './pages/abusers/abusers.component';
import {MatTableModule} from "@angular/material/table";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatButtonModule} from "@angular/material/button";
import {EditUserDialogComponent} from './components/edit-user-dialog/edit-user-dialog.component';
import {MatDialogModule} from "@angular/material/dialog";
import {MatFormFieldModule} from "@angular/material/form-field";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatInputModule} from "@angular/material/input";
import {AcknowledgmentComponent} from './components/acknowledgment/acknowledgment.component';
import {EditCompanyDialogComponent} from './components/edit-company-dialog/edit-company-dialog.component';
import {AddUserDialogComponent} from './components/add-user-dialog/add-user-dialog.component';
import {CreateUserComponent} from './components/create-user/create-user.component';
import {CreateCompanyComponent} from './components/create-company/create-company.component';
import {MatCardModule} from "@angular/material/card";
import {HttpClientModule} from "@angular/common/http";
import {NavbarComponent} from './components/navbar/navbar.component';
import {ToastrModule} from "ngx-toastr";
import {MatSelectModule} from "@angular/material/select";
import {MatIconModule} from "@angular/material/icon";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatNativeDateModule} from "@angular/material/core";
import {MatSortModule} from "@angular/material/sort";

@NgModule({
  declarations: [
    AppComponent,
    CompaniesComponent,
    UsersComponent,
    AbusersComponent,
    EditUserDialogComponent,
    AcknowledgmentComponent,
    EditCompanyDialogComponent,
    AddUserDialogComponent,
    CreateUserComponent,
    CreateCompanyComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatToolbarModule,
    MatButtonModule,
    MatDialogModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    MatCardModule,
    ToastrModule.forRoot(),
    HttpClientModule,
    MatSelectModule,
    FormsModule,
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSortModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
