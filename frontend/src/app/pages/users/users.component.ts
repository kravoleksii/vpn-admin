import {Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {User} from "../../models/user";
import {MatDialog} from "@angular/material/dialog";
import {EditUserDialogComponent} from "../../components/edit-user-dialog/edit-user-dialog.component";
import {Subject} from "rxjs";
import {takeUntil} from "rxjs/operators";
import {AcknowledgmentComponent} from "../../components/acknowledgment/acknowledgment.component";
import {ApiService} from "../../services/api.service";
import {ToastrService} from "ngx-toastr";
import {MatTable} from "@angular/material/table";
import {slider} from "../../animations/animations";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
  animations: [slider]
})
export class UsersComponent implements OnInit, OnDestroy {

  destroyed$: Subject<boolean> = new Subject<boolean>();
  displayedColumns = ['name', 'lastname', 'email', 'company'];
  users: User[];
  errors: string[] = [];
  showCreateWindow: boolean = false;
  @Output() onUserEdit: EventEmitter<object> = new EventEmitter<object>();
  @ViewChild(MatTable) matTable: MatTable<User>;

  constructor(
    private dialog: MatDialog,
    private api: ApiService,
    private toastr: ToastrService
  ) {
  }

  ngOnInit() {
    this.getUsers();
  }

  ngOnDestroy(): void {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }

  toggleCreateWindow(): void {
    this.showCreateWindow = !this.showCreateWindow;
  }

  getUsers(): void {
    this.api.getUsersData({})
      .pipe(takeUntil(this.destroyed$))
      .subscribe((response: User[]) => {
        this.users = response;
      });
  }

  editUser(user: User): void {
    let dialogWindow = this.dialog.open(EditUserDialogComponent, {
      width: "250px",
      data: new User().deserialize(user)
    });
    dialogWindow.afterClosed()
      .pipe(takeUntil(this.destroyed$))
      .subscribe((user: User) => {
        if (user) {
          this.api.sendEditedUserData(user)
            .subscribe(user => {
              this.toastr.success('User has been edited!');
              // let userIdx = this.users.indexOf(this.users.find(item => item.id === user.id))
              // this.users.splice(userIdx, 1, user);
              // this.matTable.renderRows();
              this.getUsers();
            }, error => {
              Object.values(error.error).forEach((err: string) => {
                this.errors.push(err)
              });
              this.toastr.warning(this.errors.join('\n'));
              this.errors = [];
            });
        }
      })
  }

  deleteUser(user: User): void {
    let dialogWindow = this.dialog.open(AcknowledgmentComponent, {
      width: "250px",
      data: `Are you sure want to delete user "${user.name}"?`
    });
    dialogWindow.afterClosed()
      .pipe(takeUntil(this.destroyed$))
      .subscribe((result: boolean) => {
        if (result) {
          this.api.deleteUser(user.id)
            .pipe(takeUntil(this.destroyed$))
            .subscribe(() => {
              this.getUsers();
            });
        }
      })
  }
}
