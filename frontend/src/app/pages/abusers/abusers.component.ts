import {Component, OnDestroy} from '@angular/core';
import {ApiService} from "../../services/api.service";
import {Subject} from "rxjs";
import {takeUntil} from "rxjs/operators";
import {FormControl} from '@angular/forms';
import {MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MatDatepicker} from '@angular/material/datepicker';
import {Company} from "../../models/company";
import * as _moment from 'moment';
// @ts-ignore
import {default as _rollupMoment, Moment} from 'moment';

const moment = _rollupMoment || _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  }
};

@Component({
  selector: 'app-abusers',
  templateUrl: './abusers.component.html',
  styleUrls: ['./abusers.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})

export class AbusersComponent implements OnDestroy {
  date = new FormControl(moment());
  destroyed$: Subject<boolean> = new Subject<boolean>();
  abusersCompanies: Company[];
  displayedColumns = ['name', 'quota', 'used'];
  disabled: boolean = false;


  constructor(
    private api: ApiService
  ) {
  }

  ngOnDestroy(): void {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }

  createTransferData(): void {
    this.disabled = true;
    this.api.createTransferData()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => {
        this.disabled = false;
      });
  }

  chosenYearHandler(normalizedYear: Moment) {
    const ctrlValue = this.date.value;
    ctrlValue.year(normalizedYear.year());
    this.date.setValue(ctrlValue);
  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.date.value;
    ctrlValue.month(normalizedMonth.month());
    this.date.setValue(ctrlValue);
    datepicker.close();
  }

  showAbusersCompanies(): void {
    this.disabled = true;
    this.api.getTransferData({
      params: {
        month: this.date.value.month(),
        year: this.date.value.year()
      }
    })
      .pipe(takeUntil(this.destroyed$))
      .subscribe((response: Company[]) => {
        this.abusersCompanies = response;
        this.disabled = false;
      });
  }
}
