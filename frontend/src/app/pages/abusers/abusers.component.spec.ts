import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AbusersComponent } from './abusers.component';

describe('AbusersComponent', () => {
  let component: AbusersComponent;
  let fixture: ComponentFixture<AbusersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AbusersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbusersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
