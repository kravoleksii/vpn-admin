import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Company} from "../../models/company";
import {MatDialog} from "@angular/material/dialog";
import {Subject} from "rxjs";
import {EditCompanyDialogComponent} from "../../components/edit-company-dialog/edit-company-dialog.component";
import {takeUntil} from "rxjs/operators";
import {AcknowledgmentComponent} from "../../components/acknowledgment/acknowledgment.component";
import {ApiService} from "../../services/api.service";
import {ToastrService} from "ngx-toastr";
import {slider} from "../../animations/animations";
import {MatSort} from "@angular/material/sort";

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.scss'],
  animations: [slider]
})
export class CompaniesComponent implements OnInit, OnDestroy {

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  destroyed$: Subject<boolean> = new Subject<boolean>();
  displayedColumns: string[] = ["name", "quota"];
  showCreateWindow: boolean = false;
  errors: string[];
  companies: Company[];

  constructor(
    private dialog: MatDialog,
    private api: ApiService,
    private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {
    this.getCompanies();
  }


  ngOnDestroy(): void {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }

  toggleCreateWindow(): void {
    this.showCreateWindow = !this.showCreateWindow;
  }

  getCompanies(): void {
    this.api.getCompaniesData({})
      .pipe(takeUntil(this.destroyed$))
      .subscribe((response: Company[]) => {
        this.companies = response;
      });
  }

  editCompany(company: Company): void {
    let dialogWindow = this.dialog.open(EditCompanyDialogComponent, {
      width: "250px",
      data: new Company().deserialize(company)
    });
    dialogWindow.afterClosed()
      .pipe(takeUntil(this.destroyed$))
      .subscribe((company: Company) => {
        if (company) {
          this.api.sendEditedCompanyData(company)
            .subscribe(() => {
              this.toastr.success('Company has been edited!');
              this.getCompanies();
            }, error => {
              Object.values(error.error).forEach((err: string) => {
                this.errors.push(err);
              });
              this.toastr.warning(this.errors.join('\n'));
              this.errors = [];
            })
        }
      })
  }

  deleteCompany(company: Company): void {
    let dialogWindow = this.dialog.open(AcknowledgmentComponent, {
      width: "250px",
      data: `Are you sure want to delete company "${company.name}?"`
    });
    dialogWindow.afterClosed()
      .pipe(takeUntil(this.destroyed$))
      .subscribe((result: boolean) => {
        if (result) {
          this.api.deleteCompany(company.id)
            .pipe(takeUntil(this.destroyed$))
            .subscribe();
          this.getCompanies();
        }
        this.getCompanies();
      })
  }
}

