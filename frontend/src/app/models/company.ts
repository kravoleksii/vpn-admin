import {Deserializable} from "../interfaces/deserializable";

export class Company implements Deserializable {
  id: number;
  name: string;
  quota: number;
  used?: number;

  quotaString(): string {
    if (this.quota <= 999999) {
      return ((this.quota / 1000) + ' GB')
    } else if (this.quota >= 1000000) {
      return ((this.quota / 1000000) + ' TB')
    }
  }

  usedString(): string {
    if (this.used <= 999999) {
      return (Math.round(this.used / 1000) + ' GB')
    } else if (this.used >= 1000000) {
      return (Math.round(this.used / 1000000) + ' TB')
    }
  }

  deserialize(input: object) {
    Object.assign(this, input);
    return this;
  }
}
