import {Company} from "./company";
import {Deserializable} from "../interfaces/deserializable";

export class User implements Deserializable {
  id: number;
  name: string;
  lastname: string;
  email: string;
  company: Company;

  deserialize(input: object) {
    Object.assign(this, input);
    this.company = new Company().deserialize(this.company);
    return this;
  }
}
