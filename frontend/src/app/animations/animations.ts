import {
  animate,
  state,
  style,
  transition,
  trigger
} from "@angular/animations";

export let slider = trigger(
  'slider', [
    state('void',
      style({transform: 'translateX(-282px)', width: '0'})),
    transition('void <=> *', [
      animate(200)
    ])
  ])
