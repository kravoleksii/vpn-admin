import {Component, EventEmitter, OnDestroy, Output} from '@angular/core';
import {ApiService} from "../../services/api.service";
import {FormBuilder, Validators} from "@angular/forms";
import {Subject} from "rxjs";
import {takeUntil} from "rxjs/operators";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-create-company',
  templateUrl: './create-company.component.html',
  styleUrls: ['./create-company.component.scss']
})
export class CreateCompanyComponent implements OnDestroy {
  companyForm = this.formBuilder.group({
    name: ['', Validators.required],
    quota: ['', Validators.required]
  });
  destroyed$: Subject<boolean> = new Subject<boolean>();
  errors: string[] = [];
  @Output() onCompanyCreate: EventEmitter<object> = new EventEmitter<object>();
  @Output() onCancel: EventEmitter<null> = new EventEmitter<null>();

  constructor(
    private api: ApiService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService
  ) {
  }

  ngOnDestroy(): void {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }

  createCompany(): void {
    if (this.companyForm.valid) {
      let companyData = {
        ...this.companyForm.value
      };
      this.api.sendNewCompanyData(companyData)
        .pipe(takeUntil(this.destroyed$))
        .subscribe(response => {
          this.toastr.success('Company has been created!');
          this.onCompanyCreate.emit(response);
        }, error => {
          Object.values(error.error).forEach((err: string) => {
            this.errors.push(err)
          });
            this.toastr.warning(this.errors.join('\n'));
            this.errors = [];
        });
    }
  }
}
