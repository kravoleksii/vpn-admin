import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {ApiService} from "../../services/api.service";
import {FormBuilder, Validators} from "@angular/forms";
import {Subject} from "rxjs";
import {ToastrService} from "ngx-toastr";
import {takeUntil} from "rxjs/operators";
import {Company} from "../../models/company";

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss'],
})
export class CreateUserComponent implements OnInit, OnDestroy {
  userForm = this.formBuilder.group({
    name: ["", Validators.required],
    lastname: ["", Validators.required],
    email: ["", [Validators.email, Validators.required]],
    company: ["", Validators.required]
  });
  destroyed$: Subject<boolean> = new Subject<boolean>();
  errors: string[] = [];
  companies: Company[];
  @Output() onUserCreate: EventEmitter<object> = new EventEmitter<object>();
  @Output() onCancel: EventEmitter<null> = new EventEmitter<null>();

  constructor(
    private api: ApiService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService
  ) {
  }

  ngOnInit() {
    this.getCompanies();
  }

  ngOnDestroy(): void {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }

  getCompanies(): void {
    this.api.getCompaniesData({})
      .pipe(takeUntil(this.destroyed$))
      .subscribe((response: Company[]) => {
        this.companies = response;
      });
  }

  createUser(): void {
    if (this.userForm.valid) {
      let userData = {
        ...this.userForm.value
      };
      this.api.sendNewUserData(userData)
        .pipe(takeUntil(this.destroyed$))
        .subscribe(response => {
          this.toastr.success('User has been created!');
          this.onUserCreate.emit(response);
        }, error => {
          Object.values(error.error).forEach((err: string) => {
            this.errors.push(err)
          });
          this.errors.forEach(error => {
            this.toastr.warning(error);
          })
          this.errors = [];
        });
    }
  }

  onChange(event, company: Company) {
    if (event.isUserInput) {
      this.userForm.controls.company.setValue(company.id)
    }
  }
}
