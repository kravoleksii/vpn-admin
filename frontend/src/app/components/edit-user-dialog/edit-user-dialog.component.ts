import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Company} from "../../models/company";
import {takeUntil} from "rxjs/operators";
import {ApiService} from "../../services/api.service";
import {Subject} from "rxjs";
import {User} from "../../models/user";

@Component({
  selector: 'app-edit-dialog',
  templateUrl: './edit-user-dialog.component.html',
  styleUrls: ['./edit-user-dialog.component.scss']
})

export class EditUserDialogComponent implements OnInit, OnDestroy {
  userForm: FormGroup;
  companies: Company[];
  destroyed$: Subject<boolean> = new Subject<boolean>();

  constructor(
    private api: ApiService,
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data,
    public dialogRef: MatDialogRef<EditUserDialogComponent>
  ) {
  }

  ngOnInit() {
    this.userForm = this.formBuilder.group({
      id: [this.data.id],
      name: [this.data.name, Validators.required],
      lastname: [this.data.lastname, Validators.required],
      email: [this.data.email, Validators.email],
      company: [this.data.company.id]
    });
    this.getCompanies();
  }

  ngOnDestroy(): void {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }

  getCompanies(): void {
    this.api.getCompaniesData({})
      .pipe(takeUntil(this.destroyed$))
      .subscribe((response: Company[]) => {
        this.companies = response;
      });
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  onConfirm(): void {
    if (this.userForm.valid) {
      this.dialogRef.close(this.userForm.value);
    }
  }

  onChange(event, company: Company) {
    if (event.isUserInput) {
      this.userForm.value.company = company.id;
    }
  }

}
