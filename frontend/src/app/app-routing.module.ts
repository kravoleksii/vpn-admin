import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CompaniesComponent} from "./pages/companies/companies.component";
import {UsersComponent} from "./pages/users/users.component";
import {AbusersComponent} from "./pages/abusers/abusers.component";


const routes: Routes = [
  {
    path: '',
    redirectTo: '/companies',
    pathMatch: 'full'
  }, {
    path: "companies",
    component: CompaniesComponent
  }, {
    path: "users",
    component: UsersComponent
  }, {
    path: "abusers",
    component: AbusersComponent
  }, {
    path: '**',
    redirectTo: '/companies',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
