import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {map} from "rxjs/operators";
import {Company} from "../models/company";
import {User} from "../models/user";

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  apiUrl = 'http://127.0.0.1:8000/api/v1beta'

  constructor(private http: HttpClient) {
  }

  getUsersData(options): any {
    return this.http.get(this.apiUrl + '/users', options)
      .pipe(map((response: any) => (
        response.map((user: User) => new User().deserialize(user))
      )));
  }

  getCompaniesData(options): any {
    return this.http.get(this.apiUrl + '/companies', options)
      .pipe(map((response: any) => (
        response.map((company: Company) => new Company().deserialize(company))
      )));
  }

  sendNewUserData(formData) {
    return this.http.post(this.apiUrl + '/create-user', formData)
      .pipe(map((response: any) => new User().deserialize(response)));
  }

  sendEditedUserData(user: User) {
    return this.http.patch(this.apiUrl + '/edit-user/' + user.id.toString(), user)
      .pipe(map((response: any) => new User().deserialize(response)));
  }

  deleteUser(userId) {
    return this.http.delete(this.apiUrl + '/delete-user/' + userId.toString())
  }

  sendNewCompanyData(formData) {
    return this.http.post(this.apiUrl + '/create-company', formData)
      .pipe(map((response: any) => new Company().deserialize(response)));
  }

  sendEditedCompanyData(company: Company) {
    return this.http.patch(this.apiUrl + '/edit-company/' + company.id.toString(), company)
      .pipe(map((response: any) => new Company().deserialize(response)));
  }

  deleteCompany(companyId) {
    return this.http.delete(this.apiUrl + '/delete-company/' + companyId.toString())
  }

  createTransferData() {
    return this.http.post(this.apiUrl + '/abusers', {})
      .pipe(map((response: any) => new Company().deserialize(response)));
  }

  getTransferData(options) {
    return this.http.get(this.apiUrl + '/abusers', options)
      .pipe(map((response: any) => (
        response.map((company: Company) => new Company().deserialize(company))
      )));
  }
}
